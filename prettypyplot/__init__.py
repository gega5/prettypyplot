"""
.. include:: ../README.md

"""

from .colors import *
from .plot import *
from .style import *
from .texts import *
from .subplots import *

__version__ = '0.7.0'
